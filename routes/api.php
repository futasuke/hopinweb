<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Api for passenger
Route::post('/try-register', 'ApiController@tryRegister')->name('tryRegister');
Route::post('/try-firstDetail', 'ApiController@tryFirstDetail')->name('tryFirstDetail');
Route::post('/try-login', 'ApiController@tryLogin')->name('tryLogin');
Route::get('/try-logout', 'ApiController@tryLogout')->name('tryLogout');
Route::post('/try-uploadImage/{userId}', 'ApiController@tryUploadImage')->name('tryUploadImage');
Route::get('/try-getUser/{userId}', 'ApiController@tryGetUser');
Route::post('/try-updateInfo/{userId}', 'ApiController@updateInfo');
Route::get('/try-getNews', 'ApiController@trygGetNews');
Route::post('/try-submitFeedback', 'ApiController@submitFeedback');
Route::post('/try-getDriverLocation', 'ApiController@getDriverLocation');
Route::post('/try-getLocationCache','ApiController@getLocationCache');
Route::post('/try-storeLocationCache','ApiController@storeLocationCache');
Route::post('/try-getLocationCoord','ApiController@getLocationCoord');
Route::post('/try-storeLocationCoord','ApiController@storeLocationCoord');
Route::post('/try-requestRide','ApiController@requestRide');
Route::post('/try-checkOffer','ApiController@checkOffer');
Route::post('/try-cancelSearch','ApiController@cancelSearch');
Route::post('/try-acceptOffer', 'ApiController@acceptOffer');
Route::post('/try-getOneDriverLocation', 'ApiController@getOneDriverLocation');
Route::post('/try-rideComplete','ApiController@rideComplete');
Route::post('/try-requestCarpool','ApiController@requestCarpool');
Route::get('/try-saveRideRoute/{id}','ApiController@saveRideRoute');
Route::post('/hiker-saveHikerRoute','ApiController@saveHikerRoute');
Route::post('/hiker-searchCarpool','ApiController@searchCarpool');
Route::post('/hiker-hikerCheckStatus','ApiController@hikerCheckStatus');
// Api for driver
Route::post('/driver-register', 'ApiDriverController@tryRegister');
Route::post('/driver-uploadImage/{userId}/{fileType}', 'ApiDriverController@uploadImage');
Route::post('/driver-login', 'ApiDriverController@tryLogin');
Route::post('/driver-firstdetail', 'ApiDriverController@storeFirstDetail');
Route::get('/driver-getDriver/{id}', 'ApiDriverController@getDriver');
Route::post('/driver-updateInfo/{id}','ApiDriverController@updateInfo');
Route::post('/driver-uploadDp/{id}','ApiDriverController@uploadDp');
Route::post('/driver-getCoord/{id}','ApiDriverController@getCoord');
Route::post('/driver-sendMarkerCoord/{id}','ApiDriverController@markerCoord');
Route::post('/driver-checkRequest','ApiDriverController@checkRequest');
Route::get('/driver-checkDistance','ApiDriverController@calculateDistance');
Route::post('/driver-turnOnService','ApiDriverController@turnOnService');
Route::post('/driver-turnOffService','ApiDriverController@turnOffService');
Route::post('/driver-sendOffer','ApiDriverController@sendOffer');
Route::post('/driver-checkOffer','ApiDriverController@checkOffer');
Route::post('/driver-cancelOffer','ApiDriverController@cancelOffer');
Route::post('/driver-passengerOnBoard','ApiDriverController@passengerOnBoard');
Route::post('/driver-getDestinationCoord','ApiDriverController@getDestinationCoord');
Route::post('/driver-jobComplete','ApiDriverController@jobComplete');

// For testing purpose
Route::post('/lion/upload','TestController@uploadImage');
Route::get('/test/getDriverLocation','TestController@getDriverLocation');
Route::get('/test/guzzle','TestController@testGuzzle');
Route::post('/test/checkPath','TestController@checkPath');
Route::get('/test/checkStatus','TestController@checkStatus');
