<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','StartController@showLogin')->name('showLogin');
Route::post('/try-login','StartController@tryLogin')->name('tryLogin');
Route::get('/try-logout','StartController@tryLogout')->name('tryLogout');
// Route::get('/register','StartController@showRegister')->name('showRegister');

//This is route in Dashboard
Route::get('/admin-dashboard/home','DashboardController@showHome')->name('showHome');
Route::get('/admin-dashboard/driverList','DashboardController@showDriverList')->name('showDriverList');
Route::get('/admin-dashboard/passengerList','DashboardController@showPassengerList')->name('showPassengerList');
Route::get('/admin-dashboard/passengerDetail/{id}','DashboardController@showPassengerDetail')->name('showPassengerDetail');
Route::get('/admin-dashboard/addNews','DashboardController@showAddNews')->name('showAddNews');
Route::post('/admin-dashboard/addNews','DashboardController@requestAddNews')->name('requestAddNews');
Route::get('/admin-dashboard/newsList', 'DashboardController@showNewsList')->name('showNewsList');
Route::get('/admin-dashboard/newsInfo/{id}','DashboardController@showNewsInfo')->name('showNewsInfo');
Route::get('/admin-dashboard/feedbackList','DashboardController@showFeedbackList')->name('showFeedbackList');
Route::get('/admin-dashboard/driverDetail/{id}','DashboardController@showDriverDetail')->name('showDriverDetail');

    //This is for ajax / get data only
    Route::get('/admin-dashboard/get-passengerList','DashboardController@getPassengerList')->name('getPassengerList');
    Route::get('/admin-dashboard/get-newsList','DashboardController@getNewsList')->name('getNewsList');
    Route::get('/admin-dashboard/get-feedbackList','DashboardController@getFeedbackList')->name('getFeedbackList');
    Route::get('/admin-dashboard/get-driverList','DashboardController@getDriverList')->name('getDriverList');
    Route::get('/admin-dashboard/driver-approve/{id}','DashboardController@approveDriver')->name('approveDriver');
    Route::get('/admin-dashboard/driver-delete/{id}','DashboardController@deleteDriver')->name('deleteDriver');

//This is for testing purpose
Route::get('/test/add-admin','TestController@createAdmin');
Route::get('/test/upload-image','TestController@showUpload');
Route::post('/test/test-upload','TestController@testUpload');
Route::get('/test/storage','ApiController@testStorage');
Route::get('/test/feedback','TestController@feedback');
Route::get('/test/uploadPdf','TestController@uploadPdf');
Route::get('/test/modelRelation','TestController@modelRelation');