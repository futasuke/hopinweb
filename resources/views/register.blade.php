<html>

<head>
    <link href="{{ asset('/css/custom.css') }}" rel="stylesheet">
    <link href="{{ asset('/bootstrap/css/bootstrap-grid.css') }}" rel="stylesheet">
    <link href="{{ asset('/bootstrap/css/bootstrap-reboot.css') }}" rel="stylesheet">
    <link href="{{ asset('/bootstrap/css/bootstrap.css') }}" rel="stylesheet">
    <script src="https://kit.fontawesome.com/c92cae009f.js" crossorigin="anonymous"></script>
</head>

<body class="login-page-background">
    <div class="container">
        <div class="register-page-modal">
            
            <!-- This is title and title border-->
            <h1 class="login-page-title">Register</h1>
            <div class="login-page-titleBorder"></div>
           
            <!-- This is email container -->
            <div class="login-page-emailContainer">
                <div class="row">
                    <div class="col-lg-2">
                        <div class="login-page-icon">
                            <i class="far fa-envelope fa-2x" style="color:white"></i>
                        </div>
                    </div>
                    <div class="col-lg-10">
                        <input type="text" name="email" class="login-page-field" placeholder="Email">
                    </div>
                </div>
            </div>

            <!-- This is username container -->
            <div class="login-page-usernameContainer">
                <div class="row">
                    <div class="col-lg-2">
                        <div class="login-page-icon">
                            <i class="far fa-user fa-2x" style="color:white"></i>
                        </div>
                    </div>
                    <div class="col-lg-10">
                        <input type="text" name="username" class="login-page-field" placeholder="Username">
                    </div>
                </div>
            </div>

            <!-- This is password container -->
            <div class="login-page-passwordContainer">
                <div class="row">
                    <div class="col-lg-2">
                        <div class="login-page-icon">
                            <i class="fas fa-key fa-2x" style="color:white"></i>
                        </div>      
                    </div>
                    <div class="col-lg-10">
                        <input type="password" name="password" class="login-page-field" placeholder="Password">
                    </div>
                </div>
            </div>

            <!-- This is retype password container -->
            <div class="login-page-retypePassword">
                <div class="row">
                    <div class="col-lg-2">
                        <div class="login-page-icon">
                            <i class="fas fa-key fa-2x" style="color:white"></i>
                        </div>
                    </div>
                    <div class="col-lg-10">
                        <input type="password" name="confirm_password" class="login-page-field" placeholder="Retype Password">
                    </div>
                </div>
            </div>


            <!-- This is login button -->
            <input type="submit" value="Register" class="login-page-loginButton">
            
            <!-- This is register button -->
            <div><input type="button" class="login-page-registerButton" value="Back" onclick="goToLogin()"></div>
               
        </div>
    </div>

    <script>
    function goToLogin(){
        window.location="{{route('showLogin')}}";
    }
    </script>
</body>

</html>