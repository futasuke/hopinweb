<html>

<head>
    <meta name="csrf-token" content="{{csrf_token()}}" />
    <link href="{{ asset('/css/custom.css') }}" rel="stylesheet">
    <link href="{{ asset('/bootstrap/css/bootstrap-grid.css') }}" rel="stylesheet">
    <link href="{{ asset('/bootstrap/css/bootstrap-reboot.css') }}" rel="stylesheet">
    <link href="{{ asset('/bootstrap/css/bootstrap.css') }}" rel="stylesheet">
    <script src="https://kit.fontawesome.com/c92cae009f.js" crossorigin="anonymous"></script>
</head>

<body class="login-page-background">
    <div class="container">
        <div class="login-page-modal">
            <form method="post" action="{{url('/try-login')}}">
                @csrf
                <!-- This is title and title border-->
                <h1 class="login-page-title">Login</h1>
                <div class="login-page-titleBorder"></div>

                <!-- This is username container -->
                <div class="login-page-usernameContainer">
                    <div class="row">
                        <div class="col-lg-2">
                            <div class="login-page-icon">
                                <i class="far fa-user fa-2x" style="color:white"></i>
                            </div>
                        </div>
                        <div class="col-lg-10">
                            <input type="text" name="email" class="login-page-field" placeholder="email">
                        </div>
                    </div>
                </div>

                <!-- This is password container -->
                <div class="login-page-passwordContainer">
                    <div class="row">
                        <div class="col-lg-2">
                            <div class="login-page-icon">
                                <i class="fas fa-key fa-2x" style="color:white"></i>
                            </div>
                        </div>
                        <div class="col-lg-10">
                            <input type="password" name="password" class="login-page-field" placeholder="Password">
                        </div>
                    </div>
                </div>


                <!-- This is login button -->
                <input type="submit" value="Login" class="login-page-loginButton">
            </form>

            <!-- This will be displayed if there is error with login -->
            @if (Session::has('msg'))
            <div class="login-page-errorMessage">
                <span style="color:red;font-weight:500;">{{ Session::get('msg') }}</span>
            </div>
            @endif
        </div>
    </div>

<!-- 
    <script src="{{asset('js/app.js')}}"></script>
    <script>
        Echo.channel('home')
            .listen('NewMessage', (e) => {
                console.log(e.message);
            });
        
    </script> -->

</body>

</html>