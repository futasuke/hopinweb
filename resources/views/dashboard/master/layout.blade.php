<!doctype html>
<html lang="en">

<head>
    <!-- <title>Sidebar 02</title> -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900" rel="stylesheet">
    <script src="https://kit.fontawesome.com/c92cae009f.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{asset('/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('/css/custom.css')}}">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.20/datatables.css"/>
</head>

<body>

    <div class="wrapper d-flex align-items-stretch">
        <nav id="sidebar">
            <div class="custom-menu">
                <button type="button" id="sidebarCollapse" class="btn btn-primary">
                    <i class="fa fa-bars"></i>
                    <span class="sr-only">Toggle Menu</span>
                </button>
            </div>
            <div class="p-4 pt-5">
                <h1><a href="#" class="logo">HopIn</a></h1>
                
                <div class="dashboard-welcome-user">Welcome,<br>{{$user->first_name}}</div>

                <ul class="list-unstyled components mb-5">
                    <!-- <li class="active">
                        <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">User</a>
                        <ul class="collapse list-unstyled" id="homeSubmenu">
                            <li>
                                <a href="#">List of user</a>
                            </li>
                            <li>
                                <a href="#">Add new user</a>
                            </li>
                            <li>
                                <a href="#">Home 3</a>
                            </li>
                        </ul>
                    </li> -->
                    <li>
                        <a href="{{route('showHome')}}">Home</a>
                    </li>
                    <li>
                        <a href="#userSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">View User</a>
                        <ul class="collapse list-unstyled" id="userSubmenu">
                            <li>
                                <a href="{{route('showDriverList')}}">Driver</a>
                            </li>
                            <li>
                                <a href="{{route('showPassengerList')}}">Passenger</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#reportSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">View Report</a>
                        <ul class="collapse list-unstyled" id="reportSubmenu">
                            <li>
                                <a href="#">Driver Misconduct</a>
                            </li>
                            <li>
                                <a href="{{route('showFeedbackList')}}">App Feedback</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#newSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">News / Highlight</a>
                        <ul class="collapse list-unstyled" id="newSubmenu">
                            <li>
                                <a href="{{route('showNewsList')}}">List News / Highlight</a>
                            </li>
                            <li>
                                <a href="{{route('showAddNews')}}">Add News / Highlight</a>
                            </li>
                        </ul>
                    </li>
                    <!-- <li>
                        <a href="#">Portfolio</a>
                    </li> -->
                    <!-- <li>
                        <a href="#">User</a>
                    </li> -->
                </ul>

                <div class="mb-5">
                    <a href="{{route('tryLogout')}}" style="color: rgba(255, 255, 255, 0.8);">Log Out</a>
				</div>
            </div>
        </nav>

        <!-- Page Content  -->
        <div id="content" class="p-4 p-md-5 pt-5">
            @yield('content')
           
        </div>
    </div>

    <script src="{{asset('/js/jquery.js')}}"></script>
    <script src="{{asset('/js/popper.min.js')}}"></script>
    <script src="{{asset('/bootstrap/js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.20/datatables.js"></script>
    <script src="{{asset('/js/main.js')}}"></script>

    @yield('custom_js')
</body>

</html>