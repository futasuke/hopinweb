@extends('dashboard.master.layout')

@section('content')
    <h2 class="mb-4" style="font-weight: bold; color: rgb(232, 114, 114)">News / Highlight List</h2>

    <table class="table table-bordered" id="news-table">
        <thead>
            <tr>
                <th>Title</th>
                <th>Description</th>
                <th>Date Created</th>
                <th>Action</th>
            </tr>
        </thead>
    </table>
@endsection

@section('custom_js')
<script>
    $(document).ready(function(){
        $('#news-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{!! route('getNewsList') !!}",
            columns: [     
                { data: 'title', name: 'title' },
                { data: 'description', name: 'description' },
                { data: 'created_at', name: 'created_at' },
                { data: 'id', name: 'id' },
            ],
            columnDefs: [ {
                targets: 3,
                data:'id',
                render: function(data){
                    url = "{{url('/admin-dashboard/newsInfo')}}"+"/"+data;
                    return "<a href="+url+">Info</a>&nbsp&nbsp&nbsp<a href=# >Delete</a>";
                },
            } ]
        });
    });
</script>
@endsection