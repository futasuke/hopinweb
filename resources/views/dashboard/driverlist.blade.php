@extends('dashboard.master.layout')

@section('content')
    <h2 class="mb-4" style="font-weight: bold; color: rgb(232, 114, 114)">Driver List</h2>

    @if (session('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif

    <table class="table table-bordered" id="driver-table">
        <thead>
            <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
    </table>
@endsection

@section('custom_js')
<script>
    $(document).ready(function(){
        $('#driver-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('getDriverList') !!}',
        columns: [     
            { data: 'first_name', name: 'first_name' },
            { data: 'last_name', name: 'last_name' },
            { data: 'email', name: 'email' },
            { data: 'phone', name: 'phone' },
            { data:'status', name:'status'},
            { data: 'id', name: 'id' },
        ],
        columnDefs: [ {
            targets: 5,
            data:'id',
            render: function(data){
                url = "{{url('/admin-dashboard/driverDetail')}}"+"/"+data;
                return "<a href="+url+">Info</a>";
            },
        } ]
    });
    });
</script>

@endsection