@extends('dashboard.master.layout')

@section('content')
    <h2 class="mb-4" style="font-weight: bold; color: rgb(232, 114, 114)">Passenger List</h2>

    <table class="table table-bordered" id="passenger-table">
        <thead>
            <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Action</th>
            </tr>
        </thead>
    </table>
@endsection

@section('custom_js')
<script>
$(document).ready( function () {
    $('#passenger-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('getPassengerList') !!}',
        columns: [     
            { data: 'first_name', name: 'first_name' },
            { data: 'last_name', name: 'last_name' },
            { data: 'email', name: 'email' },
            { data: 'phone', name: 'phone' },
            { data: 'id', name: 'id' },
        ],
        columnDefs: [ {
            targets: 4,
            data:'id',
            render: function(data){
                url = "{{url('/admin-dashboard/passengerDetail')}}"+"/"+data;
                return "<a href="+url+">Info</a>&nbsp&nbsp&nbsp<a href=# >Delete</a>";
            },
        } ]
    });
});
</script>
@endsection