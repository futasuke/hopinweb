@extends('dashboard.master.layout')

@section('content')
    <h2 class="mb-4" style="font-weight: bold; color: rgb(232, 114, 114)">News : {{$news->title}}</h2>

    <image src="{{asset($news->picPath)}}"></image>

    <p>{!!nl2br($news->description)!!}</p>
@endsection

@section('custom_js')

@endsection