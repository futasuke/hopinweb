@extends('dashboard.master.layout')

@section('content')
<h2 class="mb-4" style="font-weight: bold; color: rgb(232, 114, 114)">Add News / Highlight</h2>

<div style="border-width:2px;border-color:black;border-style:solid;padding-left:20px;padding-top:10px;padding-right:20px">
    <form method="post" action="{{route('requestAddNews')}}" enctype="multipart/form-data">
        @csrf
        <div>
            <span style="font-weight:bold;color:black">Choose image banner for the news</span><br>
            <input type="file" name="file">
        </div>

        <div style="width: 100%;height:3px;background-color:rgba(0, 0, 0, 0.5);margin-top:20px"></div>

        <div style="margin-top: 20px">
            <span style="font-weight:bold;color:black">Insert news title</span><br>
            <input type="text" name="title">
        </div>

        <div style="width: 100%;height:3px;background-color:rgba(0, 0, 0, 0.5);margin-top:20px"></div>

        <div style="margin-top: 20px">
            <span style="font-weight:bold;color:black">Insert news description</span><br>
            <textarea name="description" rows="10" cols="100" overflow="auto" style="white-space: pre-line">test</textarea>
        </div>

        <div style="margin-top: 20px; margin-bottom:20px">
            <input type="submit">
        </div>
    </form>

</div>
@endsection

@section('custom_js')
@endsection