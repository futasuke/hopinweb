@extends('dashboard.master.layout')

@section('content')
    <h2 class="mb-4" style="font-weight: bold; color: rgb(232, 114, 114)">App Feedback</h2>

    <table class="table table-bordered" id="feedback-table">
        <thead>
            <tr>
                <th>Title</th>
                <th>Description</th>
                <th>User</th>
                <th>Date Submitted</th>
                <th>Action</th>
            </tr>
        </thead>
    </table>
@endsection

@section('custom_js')
<script>
   $(document).ready(function(){
        $('#feedback-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{!! route('getFeedbackList') !!}",
            columns: [     
                { data: 'title', name: 'title' },
                { data: 'description', name: 'description' },
                { data: 'user.first_name', name: 'user.first_name' },
                { data: 'created_at', name: 'created_at' },
                { data: 'id', name: 'id' },
            ],
            columnDefs: [ {
                targets: 4,
                data:'id',
                render: function(data){
                    url = "{{url('/admin-dashboard/newsInfo')}}"+"/"+data;
                    return "<a href="+url+">Info</a>&nbsp&nbsp&nbsp<a href=# >Delete</a>";
                },
            } ]
        });
    });
</script>
@endsection