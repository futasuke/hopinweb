@extends('dashboard.master.layout')

@section('content')
<h2 class="mb-4" style="font-weight: bold; color: rgb(232, 114, 114)">{{$driver->email}}'s document for approval</h2>

<!-- Approve modal  -->
<div class="modalContainer" id="approvemodal">
    <div class="modalContent">
        <span style="color:black">Are you sure you want to approve this driver?</span>
        <div class="modalButtonContainer">
            <div class="modalYesButton" id="approveThis">
                <span class="modalYesText">Approve</span>
            </div>
            <div class="modalCloseButton" id="cancelApprove">
                <span class="modalCloseText">Cancel</span>
            </div>
        </div>
    </div>
</div>

<!-- Delete modal -->
<div class="modalContainer" id="deletemodal">
    <div class="modalContent">
        <span style="color:black">Are you sure you want to delete this driver request?</span>
        <div class="modalButtonContainer">
            <div class="modalDeleteButton" id="deleteThis">
                <span class="modalDeleteText">Delete</span>
            </div>
            <div class="modalCloseButton" id="cancelDelete">
                <span class="modalCloseText">Cancel</span>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-6 previewContainer">
        <div class="previewHeader">
            <span class="headerText">Image Preview</span>
        </div>
        <div class="imgPreview">
            <image src="" id="prevImg" alt="Click on file in action section to view image"></image>
        </div>
    </div>

    <div class="actionContainer">
        <div class="previewHeader">
            <span class="headerText">Action Section</span>
        </div>
        <div class="col docLinkApproval">
            <div class="row">
                <div>
                    <span style="color:black;font-weight:bold">Date requested :</span> <span>{{$driver->document[0]->created_at}}</span>
                </div>
            </div>
            <div class="row perLinkApproval" onclick="previewImage('frontic')">
                <i class="far fa-id-badge fa-3x iconApproval"></i>
                <span class="linkLabelApproval">Front IC</span>
            </div>
            <div class="row perLinkApproval" onclick="previewImage('backic')">
                <i class="far fa-id-badge fa-3x iconApproval"></i>
                <span class="linkLabelApproval">Back IC</span>
            </div>
            <div class="row perLinkApproval" onclick="previewImage('license')">
                <i class="far fa-id-card fa-3x iconApproval"></i>
                <span class="linkLabelApproval">License</span>
            </div>
            <div class="row perLinkApproval" onclick="previewImage('carimg')">
                <i class="fas fa-car fa-3x iconApproval"></i>
                <span class="linkLabelApproval">Front Car Image</span>
            </div>
        </div>
        <div class="buttonContainerApproval">
            <div class="row">
                <div class="col approveButton" onclick="doThis('approve')">
                    <div class="approveButtonLabel">Approve</div>
                </div>
                <div class="col deleteButton" onclick="doThis('delete')">
                    <div class="deleteButtonLabel">Delete</div>
                </div>
                <div class="col emailButton" onclick="doThis('email')">
                    <div class="emailButtonLabel">Email</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('custom_js')
<script>
    // Approve part
    var approveThis = document.getElementById('approveThis');
    approveThis.onclick = function() {
        var custUrl = "{{url('/admin-dashboard/driver-approve')}}" + "/" + "{{$driver->id}}"

        $.ajax({
            url: custUrl,
            success: function(response) {
                window.location.href = "{{route('showDriverList')}}";
            }
        });
    }
    var cancelApprove = document.getElementById('cancelApprove');
    cancelApprove.onclick = function() {
        document.getElementById('approvemodal').style.display = "none";
    }

    // Delete part
    var cancelDelete = document.getElementById('cancelDelete');
    cancelDelete.onclick = function() {
        document.getElementById('deletemodal').style.display = "none";
    }
    var deleteThis = document.getElementById('deleteThis');
    deleteThis.onclick = function() {
        var custUrl = "{{url('/admin-dashboard/driver-delete')}}" + "/" + "{{$driver->id}}";

        $.ajax({
            url: custUrl,
            success: function(response) {
                window.location.href = "{{route('showDriverList')}}";
            }
        });
    }

    // General function
    function doThis(param) {
        if (param == 'email') {
            var driverEmail = "{{$driver->email}}";
            var link = 'mailto:' + driverEmail;
            window.location.href = link;
        }
        if (param == 'approve') {
            document.getElementById('approvemodal').style.display = "block"
        }

        if (param == 'delete') {
            document.getElementById('deletemodal').style.display = "block"
        }
    }

    function previewImage(param) {
        if (param == 'frontic') {
            document.getElementById("prevImg").src = "{{url($driver->document[0]->frontIcPath)}}";
        }
        if (param == 'backic') {
            document.getElementById("prevImg").src = "{{url($driver->document[0]->backIcPath)}}";
        }
        if (param == 'license') {
            document.getElementById("prevImg").src = "{{url($driver->document[0]->licensePath)}}";
        }
        if (param == 'carimg') {
            document.getElementById("prevImg").src = "{{url($driver->document[0]->carPath)}}";
        }

        console.log(param);
    }
</script>
@endsection