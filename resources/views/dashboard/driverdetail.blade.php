@extends('dashboard.master.layout')

@section('content')
    <h2 class="mb-4" style="font-weight: bold; color: rgb(232, 114, 114)">{{$driver->first_name}} {{$driver->last_name}}'s Detail</h2>

    <div class="row">
        <div class="col-xs-6">
            <image src="{{asset($driver->dpPath)}}"></image>
        </div>
        <div class="col-xs-6" style="margin-left:20px">
            <span class="passenger-detail-bold">First Name : </span> {{$driver->first_name}}<br><br>
            <span class="passenger-detail-bold">Last Name : </span> {{$driver->last_name}}<br><br>
            <span class="passenger-detail-bold">Email : </span> {{$driver->email}}<br><br>
            <span class="passenger-detail-bold">Phone : </span> {{$driver->phone}}<br><br>
        </div>
    </div>

    <h2 class="mb-4" style="font-weight: bold; color: rgb(232, 114, 114); margin-top:40px">Misconduct</h2>

    <table class="table table-bordered" id="passenger-table">
        <thead>
            <tr>
                <th>Id</th>
                <th>Title</th>
                <th>Description</th>
                <th>Date</th>
                <th>Action</th>
            </tr>
        </thead>
    </table>
@endsection

@section('custom_js')
@endsection