<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OfferRequest extends Model
{
    //
    protected $table = 'offer_request';

    public function user()
    {
        return $this->belongsTo('App\User','driver_id','id');
    }

    public function riderequest()
    {
        return $this->belongsTo('App\RideRequest','request_id','id');
    }
}
