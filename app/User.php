<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function feedback()
    {
        return $this->hasMany('App\Feedback','user_id','id');
    }

    public function document(){
        return $this->hasMany('App\Document','driver_id','id');
    }

    public function riderequest()
    {
        return $this->hasMany('App\RideRequest','passenger_id','id');
    }

    public function offerrequest()
    {
        return $this->hasMany('App\OfferRequest','driver_id','id');
    }

    public function hikerroute()
    {
        return $this->hasMany('App\HikerRoute','user_id','id');
    }
}
