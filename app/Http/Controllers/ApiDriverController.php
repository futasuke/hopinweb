<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\News;
use App\Feedback;
use App\Document;
use App\RideRequest;
use App\OfferRequest;

class ApiDriverController extends Controller
{
    //
    public function tryRegister(Request $request)
    {
        $user = User::where('email', $request->email)->first();
        $data = [];

        if (!empty($user)) {
            $data[] = [
                'message' => 'Email already exist',
            ];
            return response()->json($data);
        } else {
            if ($request->password == $request->retype) {
                $hashPassword = Hash::make($request->password);

                $newUser = new User;
                $newUser->email = $request->email;
                $newUser->password = $hashPassword;
                $newUser->role = "Driver";
                $newUser->status = "Pending";
                $newUser->save();

                $userdata = array(
                    'email' => $request->email,
                    'password' => $request->password
                );
                if (Auth::attempt($userdata)) {
                    $dataUSer = User::where('email', $request->email)->first();
                    $data[] = [
                        'message' => 'Success',
                        'content' => $dataUSer,
                    ];
                    return response()->json($data);
                } else {
                    $data[] = [
                        'message' => "Please try again",
                    ];
                    return response()->json($data);
                }
            } else {
                $data[] = [
                    'message' => "Password does not match",
                ];
                return response()->json($data);
            }
        }
    }

    public function uploadImage(Request $request, $userId, $fileType)
    {
        $folderPath = public_path('/document') . '/' . $userId;
        $user = User::where('id', $userId)->first();

        if (!File::exists($folderPath)) {
            File::makeDirectory($folderPath);

            $document = new Document;
            $user->document()->save($document);
        }

        if ($fileType == 'frontic') {
            $document = Document::where('driver_id', $userId)->first();
            $extension = $request->frontIcImg->getClientOriginalExtension();
            $document->frontIcPath = "document/" . $userId . "/frontIc." . $extension;
            $document->save();
            $request->frontIcImg->move($folderPath, 'frontIc.' . $extension);
            return response()->json($fileType);
        }
        if ($fileType == 'backic') {
            $document = Document::where('driver_id', $userId)->first();
            $extension = $request->backIcImg->getClientOriginalExtension();
            $document->backIcPath = "document/" . $userId . "/backIc." . $extension;
            $document->save();
            $request->backIcImg->move($folderPath, 'backIc.' . $extension);
            return response()->json($fileType);
        }
        if ($fileType == 'license') {
            $document = Document::where('driver_id', $userId)->first();
            $extension = $request->licenseImg->getClientOriginalExtension();
            $document->licensePath = "document/" . $userId . "/license." . $extension;
            $document->save();
            $request->licenseImg->move($folderPath, 'license.' . $extension);
            return response()->json($fileType);
        }
        if ($fileType == 'carimg') {
            $document = Document::where('driver_id', $userId)->first();
            $extension = $request->carImg->getClientOriginalExtension();
            $document->carPath = "document/" . $userId . "/carImg." . $extension;
            $document->save();
            $request->carImg->move($folderPath, 'carImg.' . $extension);
            return response()->json($fileType);
        }
    }

    public function tryLogin(Request $request)
    {
        $data = [];
        $driver = User::with('document')->where('email', $request->email)->first();
        if (!empty($driver)) {
            if (Hash::check($request->password, $driver->password)) {
                if($driver->status!="Pending"){
                    $userdata = array(
                        'email' => $request->email,
                        'password' => $request->password
                    );
                    if (Auth::attempt($userdata)) {
                        // return response()->json($driver->document);
                        if ($driver->document == '[]') {
                            $data[] = [
                                'message' => 'UploadDocument',
                                'content' => $driver,
                            ];
                        } else {
                            if ($driver->first_name == null || $driver->last_name == null || $driver->phone == null) {
                                $data[] = [
                                    'message' => 'FirstDetail',
                                    'content' => $driver,
                                ];
                            } else {
                                $data[] = [
                                    'message' => 'Success',
                                    'content' => $driver,
                                ];
                            }
                        }
                    }
                }else{
                    $data[] = [
                        'message' => 'Your account is not being approved yet',
                    ];
                }            
            } else {
                $data[] = [
                    'message' => 'Password is wrong',
                ];
            }
        } else {
            $data[] = [
                'message' => 'Email does not exist',
            ];
        }
        return response()->json($data);
    }

    public function storeFirstDetail(Request $request)
    {
        $data = [];

        $driver = User::where('id', $request->userId)->first();
        $driver->first_name = $request->fname;
        $driver->last_name = $request->lname;
        $driver->phone = $request->phone;
        $driver->save();

        $data[] = [
            'message' => 'Success',
            'content' => $driver,
        ];

        return response()->json($data);
    }

    public function getDriver($driverId){
        $user = User::where('id',$driverId)->first();
        $data[] = [
            'message' => "DpUploaded",
            'content' => $user,
        ];
        return response()->json($data);
    }

    public function updateInfo(Request $request,$driverId){
        $user = User::where('id',$driverId)->first();
        if($request->title=="First Name"){
            $user->first_name = $request->input;
            $user->save();
        }
        if($request->title=="Last Name"){
            $user->last_name = $request->input;
            $user->save();
        }
        if($request->title=="Email"){
            $user->email = $request->input;
            $user->save();
        }
        if($request->title=="Phone"){
            $user->phone = $request->input;
            $user->save();
        }
        return response()->json($user);
    }

    public function uploadDp(Request $request, $driverId){
        $driver = User::where('id',$driverId)->first();
        $ext = $request->dpImg->getClientOriginalExtension();
        $dpPath = "profilepic/".$driverId."_profilepic.".$ext;
        $driver->dpPath = $dpPath;
        $driver->save();
        $request->dpImg->move(public_path("/profilepic"),$driverId."_profilepic.".$ext);

        $data[] = [
            'message' => "DpUploaded",
            'content' => $driver,
        ];

        return response()->json($data);
    }

    public function getCoord(Request $request,$driverId){
        $driver = User::where('id',$driverId)->first();
        $driver->longitude = $request->longitude;
        $driver->latitude = $request->latitude;

        $driver->save();
    }

    public function markerCoord(Request $request, $driverId){
        $driver = User::where('id',$driverId)->first();
        $driver->longitude = $request->longitude;
        $driver->latitude = $request->latitude;

        $driver->save();

        return response()->json("saved");
    }

    public function checkRequest(Request $request){
        $data = [];

        $tempLat = $request->lati;

        $tempAngular = 5 / 6371;

        $angular = rad2deg($tempAngular);

        $tempMaxLat = $tempLat + $angular;
        $tempMinLat = $tempLat - $angular;

        $deltaLng = rad2deg(asin($tempAngular) / cos(deg2rad($tempLat)));

        $maxLon = $request->long + $deltaLng;
        $minLon = $request->long  - $deltaLng;

        $request = RideRequest::with('User','LocationCache')->where('pickup_lat', '<=', $tempMaxLat)->where('pickup_lat', '>=', $tempMinLat)
        ->where('pickup_lng', '<=', $maxLon)->where('pickup_lng', '>=', $minLon)->where('status', 'waiting')->get();

        $data[]=[
            'message'=>'List of request within 5km',
            'content'=>$request,
        ];

        return response()->json($data);
    }


    // Tak guna pun yg ni. buat penat je buat.
    public function calculateDistance(){
            $lat1 = 3.164286075005019;
            $lat2 = 3.2346714409752932;
            $lon1 = 101.70147126540542;
            $lon2 = 101.70094622299075;

            $distanceLat = ($lat1 * pi() / 180) - ($lat2 * pi() / 180);
            $distanceLon = ($lon1 * pi() / 180) - ($lon2 * pi() / 180);

            $a = sin($distanceLat / 2) * sin($distanceLat / 2) + cos($lat1 * pi() / 180) * cos($lat2 * pi() / 180) * sin($distanceLon / 2) * sin($distanceLon / 2);
            $c = 2 * atan2(sqrt($a), sqrt(1 - $a));
            $distance = 6371 * $c;

            return $distance;

            // if ($distance < 5.0) {
            //     $data[] = [
            //         'message' => "hihihihihihi",
            //         'content' => $d,
            //     ];
            // }
    }

    public function turnOnService(Request $request){
        $driver = User::where('id',$request->driverId)->first();
        $driver->driver_online = 'yes';
        $driver->save();
        
        return response()->json($driver);
    }

    public function turnOffService(Request $request){
        $driver = User::where('id',$request->driverId)->first();
        $driver->driver_online = 'no';
        $driver->save();
        
        return response()->json($driver);
    }

    public function sendOffer(Request $request){
        $driver = User::where('id',$request->driverId)->first();

        $offer = new OfferRequest;
        $offer->request_id = $request->requestId;
        $offer->driver_id = $request->driverId;
        $offer->price = $request->price;
        $offer->save();

        // $tempOffer = OfferRequest::where('driver_id',$request->driverId)->where('status','waiting')->first();

        return response()->json($offer);
    }

    public function checkOffer(Request $request){
        /*
            - check if driver request status is 'waiting' or 'picked'
            - check if offer status is 'waiting' or 'accepted'
        */
        $data=[];

        $offer = OfferRequest::where('id',$request->offerId)->first();
        $rideReq = RideRequest::where('id',$request->requestId)->first();

        if($offer->status == 'accepted' && $rideReq->status == 'picked'){
            $data[]=[
                'message'=>'Offer Accepted',
                'content'=>$rideReq,
            ];
        }

        if($offer->status == 'waiting' && $rideReq->status == 'picked'){
            $data[]=[
                'message'=>'Offer Rejected',
            ];
        }

        if($offer->status == 'waiting' && $rideReq->status == 'waiting'){
            $data[]=[
                'message'=>'Refresh',
            ];
        }

        if(empty($offer)&&empty($rideReq)){
            $data[]=[
                'message'=>'Request Cancel',
            ];
        }

        return response()->json($data);
    }

    public function cancelOffer(Request $request){
        $offer = OfferRequest::where('id',$request->offerId)->first();
        $offer->delete();

        return response()->json();
    }

    public function passengerOnBoard(Request $request){
        $rideReq = RideRequest::where('id',$request->ride_id)->first();
        $rideReq->status = 'otw';
        $rideReq->save();
        $data=[];
        $data[]=[
            'latitude'=>$rideReq->locationcache->latitude,
            'longitude'=>$rideReq->locationcache->longitude,
        ];
        return response()->json($data);
    }

    public function getDestinationCoord(Request $request){
        $rideReq = RideRequest::where('id',$request->ride_id)->first();
        $data = [];
        $data[]=[
            'latitude'=>$rideReq->locationcache->latitude,
            'longitude'=>$rideReq->locationcache->longitude,
        ];

        return response()->json($data);
    }

    public function jobComplete(Request $request){
        $rideReq = RideRequest::where('id',$request->ride_id)->first();
        $rideReq->status = 'complete';
        $rideReq->save();

        return response()->json('Job complete');
    }
}
