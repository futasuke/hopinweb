<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\News;
use App\Feedback;
use App\LocationCache;
use App\RideRequest;
use App\OfferRequest;
use App\RideRoute;
use GuzzleHttp\Client;
use App\HikerRoute;

class ApiController extends Controller
{
    //
    public function tryRegister(Request $request)
    {
        // return response()->json($request);

        $user = User::where('phone', $request->phone)->first();
        $data = [];
        if (!empty($user)) {
            $data[] = [
                'message' => 'Phone already exist',
            ];
            return response()->json($data);
        } else {
            $newUser = new User;
            $newUser->phone = $request->phone;
            // $newUser->password = "";
            $newUser->role = "Passenger";
            $newUser->save();

            $dataUSer = User::where('phone', $request->phone)->first();
            $data[] = [
                'message' => 'Success',
                'content' => $dataUSer,
            ];
            return response()->json($data);

            /*
                Old code where register must use password and email
            */

            // if ($request->password == $request->retype) {
            //     $hashPassword = Hash::make($request->password);

            //     $newUser = new User;
            //     $newUser->email = $request->email;
            //     $newUser->password = $hashPassword;
            //     $newUser->role = "Passenger";
            //     $newUser->save();

            //     $userdata = array(
            //         'email' => $request->email,
            //         'password' => $request->password
            //     );
            //     if (Auth::attempt($userdata)) {
            //         $dataUSer = User::where('email', $request->email)->first();
            //         $data[] = [
            //             'message' => 'Success',
            //             'content' => $dataUSer,
            //         ];
            //         return response()->json($data);
            //     } else {
            //         $data[] = [
            //             'message' => "Please try again",
            //         ];
            //         return response()->json($data);
            //     }
            // } else {
            //     $data[] = [
            //         'message' => "Password does not match",
            //     ];
            //     return response()->json($data);
            // }
        }
    }

    public function tryFirstDetail(Request $request)
    {
        $data = [];

        $checkEmail = User::where('email', $request->email)->first();

        if (empty($checkEmail)) {
            $user = User::where('id', $request->userId)->first();
            $user->first_name = $request->fname;
            $user->last_name = $request->lname;
            $user->email = $request->email;
            $user->gender = $request->gender;
            $user->save();

            $data[] = [
                'message' => "Success",
                'content' => $user,
            ];
            return response()->json($data);
        } else {
            $data[] = [
                'message' => "Email already exist",
            ];
            return response()->json($data);
        }
    }

    public function tryLogin(Request $request)
    {
        $data = [];
        $user = User::where('phone', $request->phone)->first();
        if (!empty($user)) {
            if ($user->first_name == null || $user->last_name == null || $user->email == null) {
                $data[] = [
                    'message' => "FirstDetail",
                    'content' => $user,
                ];
            } else {
                $data[] = [
                    'message' => "Success",
                    'content' => $user,
                ];
            }
        } else {
            $data[] = [
                'message' => "Phone number does not exist"
            ];
        }

        /*
            Old code where login must use email and password
        */

        // $user = User::where('email', $request->email)->first();
        // if (!empty($user)) {
        //     if (Hash::check($request->password, $user->password)) {
        //         $userdata = array(
        //             'email' => $request->email,
        //             'password' => $request->password
        //         );
        //         if (Auth::attempt($userdata)) {
        //             $dataUSer = User::where('email', $request->email)->first();
        //             if ($user->first_name == null || $user->last_name == null || $user->phone == null) {
        //                 $data[] = [
        //                     'message' => "FirstDetail",
        //                     'content' => $dataUSer,
        //                 ];
        //             } else {
        //                 $data[] = [
        //                     'message' => "Success",
        //                     'content' => $dataUSer,
        //                 ];
        //             }
        //         }
        //     } else {
        //         $data[] = [
        //             'message' => "Password is wrong"
        //         ];
        //     }
        // } else {
        //     $data[] = [
        //         'message' => "Email does not exist"
        //     ];
        // }
        return response()->json($data);
    }

    public function tryLogout()
    {
        $data = [];
        Auth::logout();
        $data[] = [
            'message' => "Success"
        ];
        return response()->json($data);
    }

    public function tryUploadImage(Request $request, $userId)
    {
        $id = $userId;
        $user = User::where('id', $id)->first();
        $extension = $request->image->getClientOriginalExtension();
        $path = 'profilepic/' . $id . '_profilepic.' . $extension;
        if (File::exists(public_path($path)) || $user->dpPath == "profilepic/default_profile.jpg") {
            File::delete(public_path($path));
            $request->image->move(public_path('/profilepic'), $id . "_profilepic." . $extension);
            $user->dpPath = $path;
            $user->save();
        } else {
            $request->image->move(public_path('/profilepic'), $id . "_profilepic." . $extension);
            $user->dpPath = $path;
            $user->save();
        }

        // $request->image->storeAs('profilepic', $id . "_profilepic".random_int(0,999).".".$extension);
        // $user->dpPath = $path;
        // $user->save();

        $data[] = [
            'message' => "DpUploaded",
            'content' => $user,
        ];

        // print_r($request->file('image'));
        return response()->json($data);
    }

    public function testStorage()
    {
        $data = [];
        // $path = url("/storage/profilepic/1_profilepic.jpg");
        $id = 1;
        $user = User::where('id', $id)->first();
        dd($user);
        $path = 'profilepic/' . $id . '_profilepic.jpg';
        if (File::exists(public_path($path))) {
            dd('File is Exists');
        } else {
            dd('File is Not Exists');
        }
    }

    public function tryGetUser($userId)
    {
        $user = User::where('id', $userId)->first();
        $data[] = [
            'message' => "DpUploaded",
            'content' => $user,
        ];
        return response()->json($data);
    }

    public function updateInfo(Request $request, $userId)
    {
        $user = User::where('id', $userId)->first();
        if ($request->title == "First Name") {
            $user->first_name = $request->input;
            $user->save();
        }
        if ($request->title == "Last Name") {
            $user->last_name = $request->input;
            $user->save();
        }
        if ($request->title == "Email") {
            $user->email = $request->input;
            $user->save();
        }
        if ($request->title == "Phone") {
            $user->phone = $request->input;
            $user->save();
        }
        return response()->json($user);
    }

    public function trygGetNews()
    {
        $news = News::all();
        $data[] = [
            'message' => "Getting news",
            'content' => $news,
        ];

        return response()->json($data);
    }

    public function submitFeedback(Request $request)
    {
        $user = User::where('id', $request->user_id)->first();

        $feedback = new Feedback;
        $feedback->title = $request->title;
        $feedback->description = $request->description;
        $feedback->suggestion = $request->suggestion;

        $user->feedback()->save($feedback);

        $data[] = [
            'message' => "Submit success",
        ];

        return response()->json($data);
    }

    public function getDriverLocation(Request $request)
    {
        // $driver = User::where('longitude', '!=', null)->where('latitude', '!=', null)->get();
        // $test = User::where('id',37)->first();

        $data = [];

        $tempLat = $request->lati;

        $tempAngular = 5 / 6371;

        $angular = rad2deg($tempAngular);

        $tempMaxLat = $tempLat + $angular;
        $tempMinLat = $tempLat - $angular;

        $deltaLng = rad2deg(asin($tempAngular) / cos(deg2rad($tempLat)));

        $maxLon = $request->long + $deltaLng;
        $minLon = $request->long  - $deltaLng;

        // if(asin($angular) / cos($tempLat) < 0) $driver = User::where('latitude', '<=', $tempMaxLat)->where('longitude', '<=', $minLon)->where('role', 'Driver')->get();
        // if(asin($angular) / cos($tempLat) > 0) $driver = User::where('latitude', '<=', $tempMaxLat)->where('longitude', '<=', $maxLon)->where('role', 'Driver')->get();

        $driver = User::where('latitude', '<=', $tempMaxLat)->where('latitude', '>=', $tempMinLat)
            ->where('longitude', '<=', $maxLon)->where('longitude', '>=', $minLon)->where('role', 'Driver')->get();

        // $check = $test->latitude;
        // if($check<=$tempMinLat){
        //     return 'bruh';
        // }

        $data[] = [
            'message' => "List of drivers within ",
            'content' => $driver,
        ];

        /*
            Old code. Not efficient.
        */
        // foreach ($driver as $d) {
        // $lat2 = $d->latitude;
        // $lon2 = $d->longitude;

        // $distanceLat = ($tempMaxLat * pi() / 180) - (3.2346175475177903 * pi() / 180);
        // $distanceLon = (104.28215282441 * pi() / 180) - (101.70080473646522 * pi() / 180);

        // $a = sin($distanceLat / 2) * sin($distanceLat / 2) + cos($tempMaxLat * pi() / 180) * cos(3.2346175475177903 * pi() / 180) * sin($distanceLon / 2) * sin($distanceLon / 2);
        // $c = 2 * atan2(sqrt($a), sqrt(1 - $a));
        // $distance = 6371 * $c;

        // return $distance;

        // if ($distance < 5.0) {
        //     $data[] = [
        //         'message' => "hihihihihihi",
        //         'content' => $d,
        //     ];
        // }
        // }


        return response()->json($data);
        // return (asin($angular) / cos(($tempLat)));
        // return "Max lat : ".$tempMaxLat."  Max lng : ".$maxLon;

        // return asin(sin($angular) /cos($tempLat));
    }

    public function getLocationCache(Request $request)
    {
        $data = [];
        $location = LocationCache::where('description', 'like', '%' . $request->destination . '%')->where('description', 'like', '%Malaysia%')->get();
        if (!$location->isEmpty()) {
            $data[] = [
                'message' => "Success",
                'content' => $location,
            ];
        } else {
            $data[] = [
                'message' => "GetGoogle",
            ];
        }

        return response()->json($data);
    }

    public function storeLocationCache(Request $request)
    {
        foreach ($request->result as $arr) {
            $location = LocationCache::where('location_id', $arr['id'])->first();
            if (empty($location)) {
                $storeL = new LocationCache;
                $storeL->location_id = $arr['place_id'];
                $storeL->description = $arr['description'];
                $storeL->main_text = $arr['structured_formatting']['main_text'];
                $storeL->secondary_text = $arr['structured_formatting']['secondary_text'];
                $storeL->save();
            }
        }

        return response()->json("Success");
        // return response()->json($request->result);
        // return response()->json($i);
    }

    public function getLocationCoord(Request $request)
    {
        $location = LocationCache::where('location_id', $request->location_id)->first();
        $data = [];

        if (empty($location)) {
            $data[] = [
                'message' => "Location_id not found",
            ];
            return response()->json($data);
        }

        if ($location->latitude == null || $location->longitude == null) {
            $data[] = [
                'message' => "GetGoogle",
            ];
            return response()->json($data);;
        }

        $data[] = [
            'message' => 'Success',
            'content' => $location,
        ];

        return response()->json($data);
    }

    public function storeLocationCoord(Request $request)
    {
        $location = LocationCache::where('location_id', $request->location_id)->first();

        if (empty($location)) {
            return response()->json('Location_id not found');;
        }

        $location->latitude = $request->latitude;
        $location->longitude = $request->longitude;
        $location->save();

        return response()->json('Success');
    }

    public function requestRide(Request $request)
    {
        $location = LocationCache::where('location_id', $request->destinationId)->first();
        $passenger = User::where('id', $request->passengerId)->first();

        $req = new RideRequest;
        $req->passenger_id = $request->passengerId;
        $req->destination_id = $request->destinationId;
        $req->pickup_lat = $request->pickupCoord['latitude'];
        $req->pickup_lng = $request->pickupCoord['longitude'];
        $req->status = 'waiting';
        $req->type = 'normal';
        $passenger->riderequest()->save($req);

        // $tempReq = RideRequest::where('passenger_id',$request->passengerId)->where('status','waiting')->first();

        return response()->json($req);
    }

    public function cancelSearch(Request $request)
    {
        $rideReq = RideRequest::where('id', $request->requestId)->first();
        $rideReq->offerrequest()->delete();
        $rideReq->delete();

        return response()->json('Request deleted');
    }

    public function checkOffer(Request $request)
    {
        // here we check if there is any offer

        $offer = OfferRequest::where('request_id', $request->requestId)->first();

        if (!empty($offer)) {
            $offer = OfferRequest::with('User')->where('request_id', $request->requestId)->get();
        }

        $data[] = [
            'content' => $offer
        ];

        return response()->json($data);
    }

    public function acceptOffer(Request $request)
    {
        /*
            - we change offer status to 'accepted'
                - offerId
            - we change request status to 'picked'
                - requestId
                - optionally can use passengerId because one passenger only have one request.
                - thus, passengerId is also unique.
            - send driver details that will come and picked passenger.
        */

        $offer = OfferRequest::with('User')->where('id', $request->offerId)->first();
        $rideReq = RideRequest::where('id', $request->requestId)->first();

        $offer->status = "accepted";
        $offer->save();
        $rideReq->status = "picked";
        $rideReq->driver_id = $offer->driver_id;
        $rideReq->save();

        $data = [];
        $data[] = [
            'message' => 'Offer Accepted',
            'content' => $offer,
        ];

        return response()->json($data);
    }

    public function getOneDriverLocation(Request $request)
    {
        $driver = User::where('id', $request->driverId)->first();
        $reqStatus = RideRequest::where('id', $request->requestId)->first();
        $data = [];
        $data[] = [
            'message' => 'Driver location',
            'content' => ['latitude' => $driver->latitude, 'longitude' => $driver->longitude],
            'reqStatus' => $reqStatus,
        ];

        return response()->json($data);
    }

    public function rideComplete(Request $request)
    {
        $rideReq = RideRequest::where('id', $request->reqId)->first();
        if ($rideReq->status == 'complete') {
            return response()->json('Complete');
        }
        return response()->json('Otw');
    }

    public function requestCarpool(Request $request)
    {
        $location = LocationCache::where('location_id', $request->destinationId)->first();
        $passenger = User::where('id', $request->passengerId)->first();

        $req = new RideRequest;
        $req->passenger_id = $request->passengerId;
        $req->destination_id = $request->destinationId;
        $req->pickup_lat = $request->pickupCoord['latitude'];
        $req->pickup_lng = $request->pickupCoord['longitude'];
        $req->status = 'waiting';
        $req->type = 'carpool';
        $passenger->riderequest()->save($req);

        // $tempReq = RideRequest::where('passenger_id',$request->passengerId)->where('status','waiting')->first();

        return response()->json($req);
    }

    public function saveRideRoute($requestId)
    {
        $req = RideRequest::with('LocationCache')->where('id', $requestId)->first();

        $ride_steps = [];
        $client = new Client();
        $url = 'https://maps.googleapis.com/maps/api/directions/json?origin=' . $req->pickup_lat . ',' . $req->pickup_lng . '&destination=' . $req->LocationCache->latitude . ',' . $req->LocationCache->longitude . '&key=AIzaSyDtafZr2PyN-K1fg-zBwsvqS-W2Cqz1IuI';
        $response = $client->request('GET', $url, ['verify' => false]);
        $test = json_decode($response->getBody()->getContents(), true);
        $tempStep = $test['routes'][0]['legs'][0]['steps'];
        $count = 0;
        foreach ($tempStep as $index => $s) {
            if ($index == 0) {
                $ride_steps[] = [
                    'lat' => $s['start_location']['lat'],
                    'lng' => $s['start_location']['lng']
                ];
            }

            $ride_steps[] = [
                'lat' => $s['end_location']['lat'],
                'lng' => $s['end_location']['lng']
            ];
        }

        $jsonEn = json_encode($ride_steps);

        $route = new RideRoute;
        $route->request_id = $requestId;
        $route->route = $jsonEn;
        $route->save();

        return response()->json('Route save Success');
    }

    public function saveHikerRoute(Request $request)
    {
        $rideReq = new RideRequest;
        $rideReq->passenger_id = $request->hikerId;
        $rideReq->pickup_lat = $request->latitude;
        $rideReq->pickup_lng = $request->longitude;
        $rideReq->destination_id = $request->locationId;
        $rideReq->type = 'hiker';
        $rideReq->status = 'waiting';
        $rideReq->save();

        $hiker = User::where('id', $request->hikerId)->first();
        $destination = LocationCache::where('location_id', $request->locationId)->first();

        $hiker_steps = [];
        $client = new Client();
        $url = 'https://maps.googleapis.com/maps/api/directions/json?origin=' . $request->latitude . ',' . $request->longitude . '&destination=' . $destination->latitude . ',' . $destination->longitude . '&key=AIzaSyDtafZr2PyN-K1fg-zBwsvqS-W2Cqz1IuI';
        $response = $client->request('GET', $url, ['verify' => false]);
        $test = json_decode($response->getBody()->getContents(), true);
        $tempStep = $test['routes'][0]['legs'][0]['steps'];
        $count = 0;
        foreach ($tempStep as $index => $s) {
            if ($index == 0) {
                $hiker_steps[] = [
                    'lat' => $s['start_location']['lat'],
                    'lng' => $s['start_location']['lng']
                ];
            }

            $hiker_steps[] = [
                'lat' => $s['end_location']['lat'],
                'lng' => $s['end_location']['lng']
            ];
        }

        $jsonEn = json_encode($hiker_steps);

        $route = new HikerRoute;
        $route->user_id = $request->hikerId;
        $route->route = $jsonEn;
        $route->save();

        $data = [];
        $data[] = [
            'newRoute' => $route,
            'newRequest' => $rideReq,
        ];

        return response()->json($data);
    }

    public function searchCarpool(Request $request)
    {
        $hikerRoute = HikerRoute::where('id', $request->routeId)->first();
        $hikerCoordString = $hikerRoute->route;
        $hikerCoordArray = json_decode($hikerCoordString, true);
        $dest = RideRequest::with('RideRoute')->where('destination_id', $request->destinationId)->where('type', 'carpool')->has('RideRoute')->get();
        $data = [];
        foreach ($dest as $d) {
            $riderCoordString = $d->RideRoute->route;
            $riderCoordArray = json_decode($riderCoordString, true);
            foreach ($hikerCoordArray as $h) {
                $response = \GeometryLibrary\PolyUtil::isLocationOnPath(['lat' => $h['lat'], 'lng' => $h['lng']], $riderCoordArray, 350);
                if ($response) {
                    $data[] = [
                        'message' => 'Found',
                        'content' => $d,
                    ];
                    return $data;
                }
            }
        }
        $data[] = [
            'message' => 'Refresh',
        ];
        return $data;
    }

    public function hikerCheckStatus(Request $request)
    {
        $hikeRequest = RideRequest::where('id', $request->requestId)->first();
        $data = [];
        if ($hikeRequest->status == 'picked') {
            $driver = User::where('id', $hikeRequest->driver_id)->first();
            $data[] = [
                'message' => 'Accepted',
                'content' => $driver,
            ];
        } else {
            $data[] = [
                'message'=>'Refresh',
                'content'=>$hikeRequest,
            ];         
        }
        return response()->json($data);
    }
}
