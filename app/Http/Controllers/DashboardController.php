<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use App\User;
use App\News;
use App\Feedback;
use App\Document;

class DashboardController extends Controller
{
    //
    // protected function checkAuth(){
    //     $user = Auth::user();
    //     if(empty($user)){
    //        return redirect();
    //     }
    // }

    public function showHome(){
        $user = Auth::user();
        if(!empty($user)){
            return view('dashboard.home',compact('user'));   
        }
        return redirect('/'); 
    }

    public function showDriverList(){
        $user = Auth::user();
        if(!empty($user)){
            return view('dashboard.driverlist',compact('user'));   
        }
        return redirect('/');
    }

    public function showPassengerList(){
        $user = Auth::user();
        if(!empty($user)){
            return view('dashboard.passengerlist',compact('user'));   
        }
        return redirect('/');
    }

    public function getPassengerList(){
        $passenger = User::where('role','Passenger')->get();
        return datatables()->of($passenger)->make();
    }

    public function showPassengerDetail($id){
        $user = Auth::user();
        if(!empty($user)){
            $passenger = User::where('id',$id)->first();
            return view('dashboard.passengerdetail',compact('user','passenger'));   
        }
        return redirect('/');
    }

    public function showAddNews(){
        $user = Auth::user();
        if(!empty($user)){
            return view('dashboard.addNews',compact('user'));   
        }
        return redirect('/');
    }

    public function requestAddNews(Request $request){
        $addNews = new News;
        $addNews->title = $request->title;
        $addNews->description = $request->description;
        $addNews->save();

        $extension = $request->file('file')->getClientOriginalExtension();
        $path = 'newspic/'.$addNews->id.'_newspic.'.$extension;
        if (File::exists(public_path($path))||$addNews->picPath=="newspic/default_picture.jpg") {
            File::delete(public_path($path));
            $request->file('file')->move(public_path('/newspic'), $addNews->id . "_newspic.".$extension);
            $addNews->picPath = $path;
            $addNews->save();
        }
        else{
            $request->file('file')->move(public_path('/newspic'), $addNews->id . "_newspic." . $extension);
            $addNews->picPath = $path;
            $addNews->save();            
        } 

        return redirect()->back();
    }

    public function showNewsList(){
        $user = Auth::user();
        if(!empty($user)){
            return view('dashboard.viewNewsList',compact('user'));   
        }
        return redirect('/');
    }

    public function getNewsList(){
        $news = News::all();
        return datatables()->of($news)->make();
    }

    public function showNewsInfo($id){
        $user = Auth::user();
        if(!empty($user)){
            $news = News::where('id',$id)->first();
            return view('dashboard.viewNewsInfo',compact('user','news'));   
        }
        return redirect('/');
    }

    public function showFeedbackList(){
        $user = Auth::user();
        if(!empty($user)){
            return view('dashboard.viewFeedback',compact('user'));   
        }
        return redirect('/');
    }

    public function getFeedbackList(){
        $feedback = Feedback::with('user')->get();
        return datatables()->of($feedback)->make();
    }

    public function getDriverList(){
        $driver = User::where('role','Driver')->get();
        return datatables()->of($driver)->make();
    }

    public function showDriverDetail($userId){
        $user = Auth::user();
        if(!empty($user)){
            $driver = User::with('Document')->where('id',$userId)->first();
            if($driver->status == 'Success'){
                return view('dashboard.driverdetail',compact('user','driver'));
            }
            if($driver->status == 'Pending'){
                if($driver->document == "[]"){
                    return redirect()->back()->with('message','User does not have any info or document yet.');
                }else{
                    return view('dashboard.driverApproval',compact('user','driver'));
                }
            }               
        }       
    }

    public function approveDriver($driverId){
        $user = Auth::user();
        if(!empty($user)){
            $driver = User::where('id',$driverId)->first();
            $driver->status = "Success";
            $driver->save();

            return response()->json("Success");
        }else{
            return response()->json("Fail");
        }
    }

    public function deleteDriver($driverId){
        $user = Auth::user();
        if(!empty($user)){
            $driver = User::where('id',$driverId)->first();
            $document = Document::where('driver_id',$driverId)->first();
            $driver->document()->delete();
            $driver->delete();

            return response()->json("Success");
        }else{
            return response()->json("Fail");
        }
    }
}
