<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;

class StartController extends Controller
{
    //

    public function showLogin(){
        return view('login');
    }

    public function showRegister(){
        return view('register');
    }

    public function tryLogin(Request $request){
        $user = User::where('email',$request->email)->first();
        if(!empty($user)){
            if(Hash::check($request->password,$user->password)){
                if($user->role=='superadmin'){
                    $userdata = array(
                        'email' => $request->email,
                        'password' => $request->password
                    );
                    if (Auth::attempt($userdata)){
                        return redirect()->route('showHome');
                    }    
                }
                else{
                    return Redirect::back()->with('msg','You do not have permission to enter this page');
                }
            }
            else{
                return Redirect::back()->with('msg','The password is wrong');
            }
        }
        else{
            return Redirect::back()->with('msg','The email is wrong or does not exist');
        }
    }

    public function tryLogout(){
        Auth::logout();
        return redirect('/');
    }
}
