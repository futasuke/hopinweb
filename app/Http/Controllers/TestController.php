<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Feedback;
use App\RideRequest;
use App\OfferRequest;
use App\RideRoute;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Http;
use GuzzleHttp\Client;
use App\HikerRoute;

class TestController extends Controller
{
    //
    public function createAdmin()
    {
        $hashPassword = Hash::make('Firdaus98');

        $user = new User;
        $user->first_name = "Muhammad Norfirdaus";
        $user->last_name = "Mohd Fadzi";
        $user->email = "firdaus531@gmail.com";
        $user->password = $hashPassword;
        $user->role = "superadmin";
        $user->phone = "0199262681";
        $user->save();
    }

    public function showUpload()
    {
        return view('upload');
    }

    public function testUpload(Request $request)
    {
        //
        // $request->
        //    dd($request->pdf);
        //    $extension = $request->pdf->getClientOriginalExtension();
        //    dd($extension);
        // $request->ic;
        // $request->kahwin;


        // if($request->kahwin);
        //     $fileName = "borang_kahwin"

        //    $getIc = $request->ic;
        $getExt = $request->pdf->getClientOriginalExtension();
        if ($getExt != "pdf" && $getExt != "PDF") {
            return redirect()->back();
        }
        // $path = 'profilepic/'.$id.'_profilepic.'.$extension;

        $pathKahwin = 'borangpermohonan/sw0102638/borangkahwin' . $getExt;
        // $pathIc = 'borangpermohonan/sw0102638/docIc';
        // $pathLesen = 'borangpermohonan/sw0102638/lesen';

        // $request->image->move(public_path('/profilepic'), $id . "_profilepic.".$extension);

        $request->pdf->move(public_path('/borangpermohonan/sw0102638'), 'borang.' . $getExt);
    }

    public function feedback()
    {
        $feedback = Feedback::with('User')->where('id', 1)->first();

        dd($feedback);
    }

    public function uploadPdf()
    {
        $pdf = \PDF::loadView('upload');
        return $pdf->stream('invoice.pdf');
        // dd('Test');
    }

    public function uploadImage(Request $request)
    {
        // $data is array;
        $testExt = [];
        $num = 0;
        // $aya = $request->uploadData

        // $ext = $request->file('image')->getClientOriginalExtension();
        foreach ($request->file('image') as $image) {
            $ext = array_push($testExt, $image->getClientOriginalExtension());
            $image->move(public_path("/lion"), 'test' . $num . "." . $image->getClientOriginalExtension());
            // array_push($testExt, $ext);
            // $image->file('image')->getClientOriginalExtension();
            $num += 1;
            // $image->move(public_path("/lion"),"test.jpeg");
            // return response()->json($image[0]);
        }

        // return response()->json($data->uploadData->get('image'));
        // return response()->json($images);
        // dd($request->file('test'));
        // return response()->json($data->uploadData);
        return response()->json($testExt);
        // return response()->json($aya);
        // if($request->hasFile('image')){
        //     return response()->json("ada file");
        // }else{
        //     // return response()->json("takde file");
        //     return response()->json($request);
        // }
    }

    public function getDriverLocation()
    {
        $user = User::where('email', 'ayam@gmail.com')->first();

        $tempLat = $user->latitude;
        $tempLng = $user->longitude;

        $angular = 5 / 6371;

        $tempMaxLat = $tempLat + $angular;
        $tempMinLat = $tempLat - $angular;

        $deltaLon = 0;
        $maxLon = $user->longitude + rad2deg(asin($angular) / cos(deg2rad($tempLat)));
        $minLon = $user->longitude  - rad2deg(asin($angular) / cos(deg2rad($tempLat)));

        $driver = User::where('latitude', '<=', $tempMaxLat)->where('longitude', '<=', $maxLon)->get();

        dd($driver);
    }

    public function modelRelation()
    {
        $request = RideRequest::with('User', 'LocationCache')->get();
        // foreach($)

        return response()->json($request);
    }

    public function testGuzzle()
    {
        // $hiker_steps = [];
        // $client = new Client();
        // $response = $client->request('GET', 'https://maps.googleapis.com/maps/api/directions/json?origin=3.234706588876179,101.70083759352565&destination=3.2349479,101.6941011&key=AIzaSyDtafZr2PyN-K1fg-zBwsvqS-W2Cqz1IuI', ['verify' => false]);
        // $test = json_decode($response->getBody()->getContents(),true);
        // $tempStep = $test['routes'][0]['legs'][0]['steps'];
        // $count = 0;
        // foreach ($tempStep as $index => $s) {
        //     if ($index == 0) {
        //         $hiker_steps[] = [
        //             'lat' => $s['start_location']['lat'],
        //             'lng' => $s['start_location']['lng']
        //         ];
        //     }

        //     $hiker_steps[] = [
        //         'lat' => $s['end_location']['lat'],
        //         'lng' => $s['end_location']['lng']
        //     ];
        // }

        // $jsonEn = json_encode($hiker_steps);

        // $route = new RideRoute;
        // $route->request_id = 110;
        // $route->route = $jsonEn;
        // $route->save();

        // return $jsonEn;

        $getRoute = RideRoute::with('RideRequest')->where('id',1)->first();
        return $getRoute;
        return json_decode($getRoute->route);
    }

    public function checkPath(Request $request){
        /*
            check destination id.
            check if carpool.
            check route.
        */
        $hikerRoute = HikerRoute::where('user_id',$request->hikerId)->first();
        $hikerCoordString = $hikerRoute->route;
        $hikerCoordArray = json_decode($hikerCoordString,true);
        $dest = RideRequest::with('RideRoute')->where('destination_id',$request->destinationId)->where('type','carpool')->has('RideRoute')->get();
        foreach($dest as $d){
            $riderCoordString = $d->RideRoute->route;
            $riderCoordArray = json_decode($riderCoordString,true);
            foreach($hikerCoordArray as $h){
                $response = \GeometryLibrary\PolyUtil::isLocationOnPath(['lat'=>$h['lat'],'lng'=>$h['lng']],$riderCoordArray,350);
                if($response){
                    return $d;
                }
            }
        }
        
        // $dest = RideRequest::where('destination_id',$request->destinationId)->where('type','carpool')->has('RideRoute')->get();
        // $hikerRoute = HikerRoute::where('user_id',1)->first();
        // $riderRoute = RideRoute::where('request_id',115)->first(); //Not needed anymore;

        // $hikerCoordString = $hikerRoute->route;
        // $hikerCoordArray = json_decode($hikerCoordString,true);

        //$riderCoordString = $riderRoute->route; //done change
        //$riderCoordArray = json_decode($riderCoordString,true); //done change

        // $count = 0;
        // foreach($hikerCoordArray as $h){
            // return $h['lat'];
        //     $response = \GeometryLibrary\PolyUtil::isLocationOnPath(['lat'=>$h['lat'],'lng'=>$h['lng']],$riderCoordArray,350);
        //     if($response){
        //         $count++;
        //     }
        // }
        // $data = [
        //     'hiker'=>$hikerCoordArray,
        //     'rider'=>$riderCoordArray,
        // ];
        // return $dest;
    }

    public function checkStatus(){
        $hikerId = 1;
        $hikeRequest = RideRequest::where('id',$hikerId)->first();
        $data=[];
        if($hikeRequest->status == 'picked'){
            $driver = User::where('id',$hikeRequest->driver_id)->first();
            $data=[
                'message'=>'Accepted',
                'content'=>$driver,
            ];
            return response()->json($data);
        }else{
            return response()->json('Refresh');
        }
    }
}
