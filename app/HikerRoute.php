<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HikerRoute extends Model
{
    //
    protected $table = 'hiker_route';

    public function user()
    {
        return $this->belongsTo('App\User','user_id','id');
    }
}
