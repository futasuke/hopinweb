<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RideRequest extends Model
{
    //
    protected $table = 'ride_request';

    public function user()
    {
        return $this->belongsTo('App\User','passenger_id','id');
    }

    public function locationcache()
    {
        return $this->belongsTo('App\LocationCache','destination_id','location_id');
    }

    public function offerrequest()
    {
        return $this->hasOne('App\OfferRequest','request_id','id');
    }

    public function rideroute()
    {
        return $this->hasOne('App\RideRoute','request_id','id');
    }
}
