<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LocationCache extends Model
{
    //
    protected $table = 'locationcache';

    public function riderequest()
    {
        return $this->hasOne('App\RideRequest','destination_id','location_id');
    }
}
