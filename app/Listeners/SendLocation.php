<?php

namespace App\Listeners;

use App\Events\TheLocation;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendLocation
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TheLocation  $event
     * @return void
     */
    public function handle(TheLocation $event)
    {
        //
        return $event;
    }
}
