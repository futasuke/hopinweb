<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RideRoute extends Model
{
    //
    protected $table = 'request_route';

    public function riderequest()
    {
        return $this->hasOne('App\RideRequest','request_id','id');
    }
}
