<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RideRequest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('ride_request', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('passenger_id');
            $table->integer('driver_id')->nullable();
            $table->string('pickup_lat');
            $table->string('pickup_lng');
            $table->string('destination_id');
            $table->string('type');
            $table->string('status');
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
